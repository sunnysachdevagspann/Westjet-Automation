/**
 * 
 */
package com.westjet.profile.testbase;

import java.lang.reflect.Method;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.westjet.profile.core.utils.Cls_Generic_methods;
import com.westjet.profile.core.utils.Config;
import com.westjet.profile.core.utils.ExcelReader;



public class TestBase
{
	protected WebDriver driver;
	String sUserName;
	String sPwd;
	public String defaultProps = "iOSsimulator.properties";
	private final Logger logger	= Logger.getLogger(TestBase.class.getName());
	protected Config config = new Config();
	
	@BeforeSuite
	@Parameters({ "envproperties" })
	public void beforeSuite(@Optional String envproperties) {
		logger.info("Started execution with " + " " + envproperties);
		if (!StringUtils.isEmpty(envproperties)) {
			config.loadProps(envproperties);
			logger.info("Environment properties recieved and preparing the environment for "
					+ envproperties);
		} else {
			config.loadProps(defaultProps);
			logger.info("Environment properties are not provided by the user ... loading the default properties");
		}
	}
	
	@BeforeClass
	public void setUp() throws Exception
	{
		String filePath = Cls_Generic_methods.returnLogFilePath();
		System.setProperty("filename", filePath);
		Cls_Generic_methods.getLoggerConfiguration();
	//	String sURL= Cls_Generic_methods.getConfigValues("URL");
		System.out.println("All configuration Successful. Demo starts now");
		driver =Cls_Generic_methods.fn_LaunchBrowser(config);
		Cls_Generic_methods.getURL(driver, config.getProperty("URL"));
		logger.info("Browser Launched ");
	}
	
	@DataProvider(name = "westjetTestData")
	public Object[][] sephoraDataProvider(Method testMethod) throws Exception {
		String sheetName = testMethod.getName();
		System.out.println( testMethod.getDeclaringClass().getName());
		String filePath = "src/test/resources/testData/TestDataSheet.xlsx";
				//+ testMethod.getDeclaringClass().getName()
				//		.replace(TestConstants.DOT, TestConstants.BACK_SLASH)
				//+ ".xlsx";
		logger.info("Test data is loaded from file " + filePath
				+ " and the sheet is " + sheetName);
		Object[][] testObjArray = ExcelReader.getTableArray(filePath,
				testMethod.getName());

		return (testObjArray);

	}
	
//	@BeforeMethod
//	public void beforeMethod(){
//		
//	}
//	
//	@AfterMethod
//	public void afterMethod(Method m) throws InterruptedException
//	{
//		m_assert.assertAll();
//		logger.info("Method : " + m.getName() + " finished");
//	}
	
	@AfterClass
	public void tearDown() throws Exception
	{
		Thread.sleep(2000);
		driver.quit();
		//oVideoRecorder.stop("Demo");
		
		
	}
}