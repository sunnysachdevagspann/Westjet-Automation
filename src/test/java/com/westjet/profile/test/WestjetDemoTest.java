/**
 * 
 */
package com.westjet.profile.test;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import com.westjet.profile.core.utils.Cls_Generic_methods;
import com.westjet.profile.pages.WestjetAdditionalInfoPage;
import com.westjet.profile.pages.WestjetHomePage;
import com.westjet.profile.pages.WestjetRegistartionConfirmedPage;
import com.westjet.profile.pages.WestjetSignInPage;
import com.westjet.profile.pages.WestjetSignUpPage;
import com.westjet.profile.testbase.TestBase;

/**
 * @author Admin
 *
 */
public class WestjetDemoTest extends TestBase {

	WestjetHomePage homePage;
	WestjetSignUpPage signUpPage;
	WestjetAdditionalInfoPage additionInfoPage;
	WestjetRegistartionConfirmedPage confirmedPage;
	WestjetSignInPage signInPage;
	private final Logger logger	= Logger.getLogger(WestjetDemoTest.class.getName());
	
	@Test(dataProvider = "westjetTestData", priority =1, enabled = true)
	public void testCreateNewAccount(String email, String pwd, String title,
			String firstName, String lastName, String address, String city,
			String country, String province, String zip, String homeAirport,
			String phoneType, String phoneNumber, String address2line) throws Exception {
		try{
			email=email.replace("jacky", "jacky"+Cls_Generic_methods.generateRandomNumber());

			homePage = new WestjetHomePage(driver);
			signUpPage = homePage.clickSignUpButton();
			additionInfoPage = signUpPage.fillPersonalInformation(email, pwd,
					title, firstName, lastName).clickNext();
			confirmedPage = additionInfoPage.typeAddressLine1(address)
					.typeCity(city).selectCountry(country)
					.selectProvinceStateRegion(province).typePostalCode(zip)
					.selectHomeAirport(homeAirport).selectPhoneType(phoneType)
					.typePhoneNumber(phoneNumber).clickAcceptTermsAndConditions()
					.clickSubmit();
			homePage = confirmedPage.clickHomeLink();
			signInPage = homePage.typeEmailId(email).typePassword(pwd)
					.clickSignIn().waitForSignInPage();
			signInPage.browseTabs();
			//signInPage.editPersonalInfo(address2line);
			signInPage.browseCalendar();
			signInPage.clickSignOut();
		}catch(Exception e){
			logger.info(Cls_Generic_methods.getErrorCause(driver));
		}

	}


	@Test(dataProvider = "westjetTestData",priority =2)
	public void testSignInBadUserAccount(String email, String pwd) throws Exception {
		System.out.println("running testSignInBadUserAccount");
		try{
			homePage = new WestjetHomePage(driver);
			//homePage = confirmedPage.clickHomeLink();
			homePage.typeEmailId(email).typePassword(pwd)
			.clickSignIn();
			signInPage = new WestjetSignInPage(driver);
			signInPage.validateErrorMesg();
		}catch(Exception e){
			logger.info(Cls_Generic_methods.getErrorCause(driver));
		}
	}

}
