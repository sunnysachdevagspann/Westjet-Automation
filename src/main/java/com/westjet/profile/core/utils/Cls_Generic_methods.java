/**
 * 
 */
package com.westjet.profile.core.utils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Driver;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.browserlaunchers.Sleeper;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverLogLevel;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
/**
 * @author Admin
 *
 */
public class Cls_Generic_methods {
	private final static Logger logger	= Logger.getLogger(Cls_Generic_methods.class.getName());
	
	public static WebDriver fn_LaunchBrowser(Config config)
			throws Exception {

		WebDriver Driver_Object = null; 
		String deviceType= config.getProperty("DeviceType");
		String BrowserType=config.getProperty("BrowserName");
		if(deviceType.isEmpty() || deviceType.equalsIgnoreCase("web"))
		{
			if (BrowserType.equalsIgnoreCase("FF")
					|| BrowserType.equalsIgnoreCase("Firefox")) {
				FirefoxProfile profile = new FirefoxProfile();
				Driver_Object = new FirefoxDriver(new FirefoxBinary(new File("/Applications/Firefox.app/Contents/MacOS/firefox-bin")),profile);
			} else if (BrowserType.equalsIgnoreCase("Safari")) {
				Driver_Object = new SafariDriver();
			} else if (BrowserType.equalsIgnoreCase("chrome")|| BrowserType.equalsIgnoreCase("Chrome")) {
				System.setProperty("webdriver.chrome.driver","src/main/Resources/drivers/chromedriver");
				DesiredCapabilities capabilities = DesiredCapabilities.chrome();
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--test-type");
//				LoggingPreferences loggingprefs = new LoggingPreferences();
//				loggingprefs.enable(LogType.BROWSER, Level.ALL);
//				capabilities.setCapability(CapabilityType.LOGGING_PREFS, loggingprefs);
//				capabilities.setCapability("chrome.binary", ".\\Resources\\Drivers\\chromedriver.exe");
				capabilities.setCapability("chrome.switches", Arrays.asList("--incognito"));
				capabilities.setCapability(ChromeOptions.CAPABILITY, options);
				Driver_Object = new ChromeDriver();
			} else if (BrowserType.equalsIgnoreCase("IE")|| BrowserType.equalsIgnoreCase("InternetExplorer")) {
				System.setProperty("webdriver.ie.driver",System.getProperty("user.dir")+"\\src\\main\\Resources\\drivers\\IEDriverServer.exe");
				InternetExplorerDriverService.Builder service = new InternetExplorerDriverService.Builder();
				service = service.withLogLevel(InternetExplorerDriverLogLevel.TRACE);
				Driver_Object = new InternetExplorerDriver(service.build());
			} else if (BrowserType.equalsIgnoreCase("remote")) {
				DesiredCapabilities cap = new DesiredCapabilities();
				cap.setBrowserName("chrome");
				Driver_Object = new RemoteWebDriver(new URL(
						"http://localhost:4446/wd/hub"), cap);
			}
			Driver_Object.manage().window().maximize();
		}else{
			if(deviceType.equals("mobile android"))
			{
				DesiredCapabilities capabilities = new DesiredCapabilities();
				capabilities.setCapability("device",config.getProperty("device"));
				capabilities.setCapability("platformName",config.getProperty("platformName"));
				capabilities.setCapability("platformVersion",config.getProperty("platformVersion"));
				capabilities.setCapability("deviceName",config.getProperty("DeviceType"));
				capabilities.setCapability("browserName",config.getProperty("BrowserName"));
				Driver_Object = new RemoteWebDriver(new URL("http://"+ config.getProperty("host") + "/wd/hub"), capabilities);
			
			} else if(deviceType.equals("simulator iOS")) {
				DesiredCapabilities capabilities = new DesiredCapabilities();
//				capabilities.setCapability("device",config.getProperty("device"));
				capabilities.setCapability("platformName",config.getProperty("platformName"));
				capabilities.setCapability("platformVersion",config.getProperty("platformVersion"));
				capabilities.setCapability("deviceName",config.getProperty("device"));
				capabilities.setCapability("browserName",config.getProperty("BrowserName"));
//				capabilities.setCapability("udid",config.getProperty("udid"));
				Driver_Object = new RemoteWebDriver(new URL("http://"+ config.getProperty("host") + "/wd/hub"), capabilities);
			} else if(deviceType.equals("mobile iOS")) {
				DesiredCapabilities capabilities = new DesiredCapabilities();
//				capabilities.setCapability("device",config.getProperty("device"));
				capabilities.setCapability("platformName",config.getProperty("platformName"));
				capabilities.setCapability("platformVersion",config.getProperty("platformVersion"));
				capabilities.setCapability("deviceName",config.getProperty("device"));
				capabilities.setCapability("browserName",config.getProperty("BrowserName"));
				capabilities.setCapability("udid",config.getProperty("udid"));
				Driver_Object = new RemoteWebDriver(new URL("http://"+ config.getProperty("host") + "/wd/hub"), capabilities);
			}
					
		}
		
		Driver_Object.manage().timeouts().implicitlyWait(Integer.parseInt(config.getProperty("ImplicitWait")),TimeUnit.SECONDS);
		return Driver_Object;
	}
	public static void getURL(WebDriver driver, String sURL)
	{
		driver.get(sURL);
		logger.info("Browser launched!!!");
	
	}
//	public static String getConfigValues(String sProperty) {
//		String sValue = "";
//		InputStream input = null;
//		try {
//			Properties prop = new Properties();
//			input = new FileInputStream(System.getProperty("user.dir")
//					+ "\\Config\\Config" + ".properties");
//			prop.load(input);
//			sValue = prop.getProperty(sProperty);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return sValue;
//	}
	/**
	 * waits and click on element
	 * 
	 * @param driver
	 * @param element
	 * @throws Exception 
	 */
	public static void clickElement(WebDriver driver, WebElement element) throws Exception {
		pauseExecutionFor(2);
		waitForElementToBeVisible(driver, element);
		if (isElementClickable(driver, element)) {
			//highlightElementBorder(driver, element);
			element.click();
			//logger.info("Clicked element "+ element);
		} else {
			throw new Exception("Element not in state of clickable");
		}
		pauseExecutionFor(2);
	}
	/**
	 * Checks if the element is clickable.
	 * 
	 * @param driver
	 * @param element
	 * @return
	 */
	private static boolean isElementClickable(WebDriver driver,
			WebElement element) {
		try {
			WebDriverWait wait = new WebDriverWait(driver,30);
			wait.until(ExpectedConditions.elementToBeClickable(element));
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}
	
	/**
	 * Selects a particular WebElement from the Select . It can be used when
	 * HTML have Select <Option> DOM
	 *
	 * @param lstElementList
	 * @param sValueToBeSelected
	 */
	public static void selectFromList(WebElement select,
			String sValueToBeSelected) {
		try {
			List<WebElement> options = select
					.findElements(By.tagName("option"));
			for (WebElement option : options) {
				if (option.getText().trim()
						.equalsIgnoreCase(sValueToBeSelected.trim())) {
					option.click();
					break;
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void selectbyValue(WebElement element,
			String sValueToBeSelected) {
		try {
			Select select = new Select(element);
			select.selectByValue(sValueToBeSelected);
			logger.info("Selected Value "+ sValueToBeSelected);
		} catch (Exception e) {
			logger.error("Something wrong !!!"+e.getMessage());
			e.printStackTrace();
		}
	}
	public static void selectbyText(WebElement element,
			String text) {
		try {
			Select select = new Select(element);
			select.selectByVisibleText(text);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * Waits and then sendkeys to element
	 * 
	 * @param driver
	 * @param element
	 * @param sValue
	 */
	public static void sendKeys(WebDriver driver, WebElement element,
			String sValue) {
		WebElement ele;
		try {
			//highlightElementBorder(driver, element);
			WebDriverWait wait = new WebDriverWait(driver,30);
			ele = wait.until(ExpectedConditions.elementToBeClickable(element));
			ele.sendKeys(sValue);
			//logger.info("Typed value  "+ sValue +" at element "+ ele);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void waitForElementToBeVisible(WebDriver driver,
			WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver,75);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	public static String returnLogFilePath() throws IOException {
		String TimeStamp = fn_GetCurrentTimeStamp();
		String FolderPath = System.getProperty("user.dir")
				+ "\\Output\\Logs\\application_" + TimeStamp + "\\";
		File FolderObj = new File(FolderPath);
		FolderObj.mkdir();
		FolderPath = FolderObj.getAbsolutePath();
		String FullFilePath = FolderPath + "\\" + "application.log";
		System.setProperty("filename", FullFilePath);
		return FullFilePath;
	}
	public static String fn_GetCurrentTimeStamp() {
		Date dte = new Date();
		DateFormat df = DateFormat.getDateTimeInstance();
		String strdte = df.format(dte);
		strdte = strdte.replaceAll(":", "_");
		return strdte;
	}
	public static void getLoggerConfiguration() {
		PropertyConfigurator.configure(System.getProperty("user.dir")+ "/Config/log4j.properties");
	}
	public static void selectFromList(List<WebElement> lstElementList,
			String sValueToBeSelected) throws Exception {
		logger.info("START OF FUNCTION->selectFromList");
		boolean flag = false;
		try {
			logger.info("Total element found --> " + lstElementList.size());
			logger.info("Value to be selected " + sValueToBeSelected
					+ " from list" + lstElementList);

			for (WebElement e : lstElementList) {
				logger.info(">>>>>>>>>>>>>" + e.getText() + "<<<<<<<<<<<<<<<");
				if (e.getText().equalsIgnoreCase(sValueToBeSelected)) {
					logger.info("Value matched in list as->" + e.getText());
					e.click();
					flag = true;
					break;
				}
			}
			if (flag == false) {
				logger.error("No match found in the list for value->"
						+ sValueToBeSelected);
				// throw new
				// Exception("No match found in the list for value->"+sValueToBeSelected);
			}
			logger.info("END OF FUNCTION->selectFromList");
		} catch (Exception e) {
			logger.error("THERE IS AN EXCEPTION ON SELECTING VALUE FROM LIST->"
					+ e.getMessage());

		}
	}
	
	public static void pauseExecutionFor(int timeoutInSeconds)
	{
		Sleeper.sleepTightInSeconds(timeoutInSeconds);
	}
	
	public static void moveToElement(WebDriver driver, WebElement element) {
		Actions action = new Actions(driver);
		action.moveToElement(element, element.getLocation().getX(), element
				.getLocation().getY());
		action.build().perform();
	}
	
	public static void mouseOverElement(WebDriver driver, WebElement element) {
		try {
			logger.info("Mouse Over Element :" + element);
			Actions builder = new Actions(driver);
			builder.moveToElement(element).build().perform();
			logger.info("Mouse Overed to Element " + element);
		} catch (Exception e) {
			logger.error("Exception ocurred while moving to element "
					+ e.getMessage());
		}
	}
	
	public void movetToElementJavascript(WebDriver driver,WebElement element) {
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",element);
	}
	/**
	 * Performs click operation using JS
	 * 
	 * @param driver
	 * @param element
	 */
	public static void clickByJS(WebDriver driver, WebElement element) {
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
	}
	public static void scrollDown(WebDriver driver) {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("scroll(0, 250)");
	}
	public static void scrollToElement(WebDriver driver, WebElement element) {
		logger.info("Scrolling to Element");
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
	}
	
	public static int generateRandomNumber(){
		int min =10;
		int max =900;
		
		Random r = new Random();
		int randNum= r.nextInt(max-min+1)+min;
		return randNum;
	}
	
	public static void highlightElementBorder(WebDriver driver,
			   WebElement element) {

			  try {
			   
			    for (int i = 0; i < 1; i++) {
			     JavascriptExecutor js = (JavascriptExecutor) driver;
			     js.executeScript(
			       "arguments[0].setAttribute('style', arguments[1]);",
			       element, "color: yellow; border: 4px solid red;");
			     // + "border: 4px solid red;");
			     Thread.sleep(600);
			     js.executeScript(
			       "arguments[0].setAttribute('style', arguments[1]);",
			       element, "");
			    }
			   

			  } catch (Exception e) {
			   logger.error("Error Occured highlighting Border " + element);
			  }
			 }
	public static void browseList(WebDriver driver,List<WebElement> lstElementList,String sValueToBeSelected) throws Exception {
		  logger.info("START OF FUNCTION->selectFromList");
		  boolean flag = false;
		  try {
		   logger.info("Total element found --> " + lstElementList.size());
		   logger.info("Value to be selected " + sValueToBeSelected
		     + " from list" + lstElementList);

		   for (WebElement e : lstElementList) {
		    highlightElementBorder(driver, e);
		    logger.info(">>>>>>>>>>>>>" + e.getText() + "<<<<<<<<<<<<<<<");
		    if (e.getText().equalsIgnoreCase(sValueToBeSelected)) {
		     logger.info("Value matched in list as->" + e.getText());
		     e.click();
		     flag = true;
		     break;
		    }
		    pauseExecutionFor(1);
		   }
		   if (flag == false) {
		    logger.error("No match found in the list for value->"
		      + sValueToBeSelected);
		    // throw new
		    // Exception("No match found in the list for value->"+sValueToBeSelected);
		   }
		   logger.info("END OF FUNCTION->selectFromList");
		  } catch (Exception e) {
		   logger.error("THERE IS AN EXCEPTION ON SELECTING VALUE FROM LIST->"
		     + e.getMessage());

		  }
		 }

	public static String getErrorCause(WebDriver driver){
		String errorMsg = null;
		try{
			WebElement element=driver.findElement(By.id("signInMaintenance"));
			if(isElementVisible(driver, element)){
				errorMsg=element.getText();
			}
		}catch(Exception e){
			errorMsg="Some Unexpected error ocurred";
		}
		return errorMsg;
	}
	private static boolean isElementVisible(WebDriver driver,
			WebElement element) {
		try {
			WebDriverWait wait = new WebDriverWait(driver,30);
			wait.until(ExpectedConditions.visibilityOf(element));
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}
}
