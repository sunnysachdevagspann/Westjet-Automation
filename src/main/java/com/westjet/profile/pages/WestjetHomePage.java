package com.westjet.profile.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import com.westjet.profile.core.utils.Cls_Generic_methods;

public class WestjetHomePage 
{
    
	protected WebDriver driver;
	private final Logger logger	= Logger.getLogger(WestjetHomePage.class.getName());
	public WestjetHomePage(WebDriver driver) {
		this.driver=driver;
		AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver,30);
		PageFactory.initElements(factory,this);
	}
	
	@FindBy(id="buttonSignUp")
	private WebElement button_SignUp;
	
	@FindBy(id="username")
	private WebElement text_EmailId;
	
	@FindBy(id="passwordHint")
	private WebElement text_PasswordLayout;
	
	@FindBy(id="password")
	private WebElement text_Password;
	
	@FindBy(id="signInSubmitLink")
	private WebElement button_SignIn;
	
	@FindBy(id="username-error")
	private WebElement text_username_error;
	
	
	public WestjetSignUpPage clickSignUpButton() throws Exception
	{
		Cls_Generic_methods.clickElement(driver, button_SignUp);
		Cls_Generic_methods.pauseExecutionFor(3);
		logger.info("clicked button signup");
		return new WestjetSignUpPage(driver);
	}
	
	public WestjetHomePage typeEmailId(String emailId){
		Cls_Generic_methods.sendKeys(driver, text_EmailId, emailId);
		logger.info("Typed email Id "+ emailId);
		return this;
	}
	
	public WestjetHomePage typePassword(String pwd) throws Exception{
		Cls_Generic_methods.clickElement(driver, text_PasswordLayout);
		Cls_Generic_methods.sendKeys(driver, text_Password, pwd);
		logger.info("typed password "+ pwd);
		return this;
	}

	public WestjetSignInPage clickSignIn() throws Exception{
		Cls_Generic_methods.scrollToElement(driver, button_SignIn);
		Cls_Generic_methods.clickByJS(driver, button_SignIn);
		Cls_Generic_methods.pauseExecutionFor(4);
		logger.info("clicked SignIn Button");
		return new WestjetSignInPage(driver);
	}
	
	public WestjetHomePage validateErrorMesg() throws Exception{
		Cls_Generic_methods.pauseExecutionFor(2);
		if(text_username_error.isDisplayed()){
			logger.info("Error mesg displayed for wrong user id : " + text_username_error.getText());
		}else{
			logger.info("Error mesg Not displayed for wrong user ID");
		}
		
		return this;
	}
}
