/**
 * 
 */
package com.westjet.profile.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import com.westjet.profile.core.utils.Cls_Generic_methods;

/**
 * @author Admin
 *
 */
public class WestjetRegistartionConfirmedPage extends WestjetHomePage {
	
	/**
	 * @param driver
	 */
	public WestjetRegistartionConfirmedPage(WebDriver driver) {
		super(driver);
		AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver,30);
		PageFactory.initElements(factory,this);
		Cls_Generic_methods.waitForElementToBeVisible(driver, label_Confirmation);
	}

	private final Logger logger	= Logger.getLogger(WestjetRegistartionConfirmedPage.class.getName());

	@FindBy(xpath="//h1[contains(text(),'Registration')]")
	private WebElement label_Confirmation;
	
	@FindBy(linkText="Home")
	private WebElement link_Home;
	
	public WestjetHomePage clickHomeLink() throws Exception
	{
		Cls_Generic_methods.clickElement(driver, link_Home);
		logger.info("clicked Home Link");
		return new WestjetHomePage(driver);
	}
	
}
