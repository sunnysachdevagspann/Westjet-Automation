/**
 * 
 */
package com.westjet.profile.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import com.westjet.profile.core.utils.Cls_Generic_methods;

/**
 * @author Admin
 *
 */
public class WestjetSignUpPage extends  WestjetHomePage{

private final Logger logger	= Logger.getLogger(WestjetSignUpPage.class.getName());
	
	public WestjetSignUpPage(WebDriver driver) {
		super(driver);
		AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver,30);
		PageFactory.initElements(factory,this);
	}
	
	@FindBy(id="email")
	private WebElement text_Email;
	
	@FindBy(id="verifyEmail")
	private WebElement text_ConfirmEmail;
	
	@FindBy(id="createPassword")
	private WebElement text_CreatePassword;
	
	@FindBy(id="verifyPassword")
	private WebElement text_ConfirmPassword;
	
	@FindBy(id="title")
	private WebElement select_Title;
	
	@FindBy(id="firstName")
	private WebElement text_FirstName;
	
	@FindBy(id="preferredName")
	private WebElement text_PreferredName;
	
	@FindBy(id="lastName")
	private WebElement text_LastName;
	
	@FindBy(id="buttonNext")
	private WebElement button_Next;

	public WestjetSignUpPage fillPersonalInformation(String emailId,String pwd,String title,String firstName,String lastName) throws Exception
	{
		Cls_Generic_methods.pauseExecutionFor(1);
		Cls_Generic_methods.sendKeys(driver, text_Email, emailId);
		logger.info("Type value at "+emailId);
		Cls_Generic_methods.sendKeys(driver, text_ConfirmEmail, emailId);
		Cls_Generic_methods.pauseExecutionFor(1);
		Cls_Generic_methods.sendKeys(driver, text_CreatePassword, pwd);
		Cls_Generic_methods.pauseExecutionFor(1);
		logger.info("Type value at "+pwd);
		Cls_Generic_methods.sendKeys(driver, text_ConfirmPassword, pwd);
		Cls_Generic_methods.pauseExecutionFor(1);
		Cls_Generic_methods.selectFromList(select_Title, title);
		Cls_Generic_methods.pauseExecutionFor(1);
		Cls_Generic_methods.sendKeys(driver, text_FirstName, firstName);
		Cls_Generic_methods.pauseExecutionFor(1);
		Cls_Generic_methods.sendKeys(driver, text_LastName, lastName);
		logger.info("Filled all the information");
		return this;
	}
	
	public WestjetAdditionalInfoPage clickNext() throws Exception
	{
		Cls_Generic_methods.moveToElement(driver, button_Next);
		//Cls_Generic_methods.clickElement(driver, button_Next);
		Cls_Generic_methods.clickByJS(driver, button_Next);
		Cls_Generic_methods.pauseExecutionFor(3);
		logger.info("Click Next Button");
		return new WestjetAdditionalInfoPage(driver);
	}
}

