/**
 * 
 */
package com.westjet.profile.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import com.westjet.profile.core.utils.Cls_Generic_methods;

/**
 * @author Admin
 *
 */
public class WestjetSignInPage extends WestjetHomePage {

	private final Logger logger	= Logger.getLogger(WestjetSignInPage.class.getName());
	public WestjetSignInPage(WebDriver driver) {
		super(driver);
		AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver,30);
		PageFactory.initElements(factory,this);
		//logger.info("Waited for link SignOut");
	}


	@FindAll({@FindBy(xpath="//ul[@class='secondaryTabs']/li/a")})
	private List<WebElement> list_SecondaryTabs;

	@FindBy(id="pkmslogoutId")
	private WebElement link_SignOut;

	@FindBy(id="personalInfoTab")
	private WebElement link_personalInfoTab;

	@FindBy(id="rewardsTab")
	private WebElement link_rewardsTab;

	@FindBy(id="manageBookingsTab")
	private WebElement link_manageBookingsTab;

	@FindBy(id="address2")
	private WebElement inputbox_address2;

	@FindBy(id="saveButton")
	private WebElement button_saveButton;

	@FindBy(xpath="//div[@class='confirmation-message']/h1")
	private WebElement text_confirmationMessage;

	@FindBy(linkText="Manage bookings")
	private WebElement link_ManageBookings;

	@FindBy(linkText="Vacations")
	private WebElement link_Vacations;

	@FindBy(xpath="//input[@value='Departure date']")
	private WebElement text_DepartureDate; 

	@FindAll({@FindBy(xpath="//div[@id='ui-datepicker-div']/div[contains(@class,'-last')]//tbody/tr/td[@data-handler='selectDay']")})
	private List<WebElement> list_Calendar;

	public WestjetSignInPage browseTabs() throws Exception
	{
		Cls_Generic_methods.clickElement(driver, link_personalInfoTab);
		logger.info("clicked tab Personal Info");
		logger.info("Page title : " + driver.getTitle());
		Cls_Generic_methods.pauseExecutionFor(2);
		Cls_Generic_methods.clickElement(driver, link_rewardsTab);
		logger.info("clicked tab Rewards ");
		logger.info("Page title : " + driver.getTitle());
		Cls_Generic_methods.pauseExecutionFor(2);
		Cls_Generic_methods.clickElement(driver, link_manageBookingsTab);
		logger.info("clicked tab Manage Bookings ");
		logger.info("Page title : " + driver.getTitle());
		return this;
	}

	public WestjetSignInPage editPersonalInfo(String address2) throws Exception
	{
		Cls_Generic_methods.clickElement(driver, link_personalInfoTab);
		logger.info("clicked tab Personal Info");
		logger.info("Page title : " + driver.getTitle());
		Cls_Generic_methods.pauseExecutionFor(3);
		Cls_Generic_methods.sendKeys(driver, inputbox_address2, address2);
		Cls_Generic_methods.pauseExecutionFor(3);
		Cls_Generic_methods.scrollToElement(driver, button_saveButton);
		Cls_Generic_methods.clickElement(driver, button_saveButton);
		Cls_Generic_methods.pauseExecutionFor(10);
		Cls_Generic_methods.waitForElementToBeVisible(driver, text_confirmationMessage);
		Cls_Generic_methods.pauseExecutionFor(3);
		if(text_confirmationMessage.isDisplayed()){
			logger.info("Confirmation displayed : " + text_confirmationMessage.getText());
		}
		return this;
	}

	public WestjetHomePage clickSignOut() throws Exception{
		Cls_Generic_methods.clickElement(driver, link_SignOut);
		logger.info("clicked link SignOut");
		return new WestjetHomePage(driver);
	}


	public WestjetSignInPage waitForSignInPage(){
		Cls_Generic_methods.waitForElementToBeVisible(driver, link_SignOut);
		return this;
	}
	
	public WestjetSignInPage browseCalendar() throws Exception{
		Cls_Generic_methods.pauseExecutionFor(3);
		  Cls_Generic_methods.clickElement(driver, link_ManageBookings);
		  Cls_Generic_methods.waitForElementToBeVisible(driver, link_Vacations);
		  Cls_Generic_methods.clickElement(driver, link_Vacations);
		  Cls_Generic_methods.pauseExecutionFor(1);
		  Cls_Generic_methods.clickElement(driver, text_DepartureDate);
		  Cls_Generic_methods.pauseExecutionFor(3);
		  Cls_Generic_methods.browseList(driver,list_Calendar, "10");
		  return this;
		 }

}
