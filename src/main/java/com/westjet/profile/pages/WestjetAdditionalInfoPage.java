/**
 * 
 */
package com.westjet.profile.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import com.westjet.profile.core.utils.Cls_Generic_methods;

/**
 * @author Admin
 *
 */
public class WestjetAdditionalInfoPage extends WestjetHomePage {

	private final Logger logger	= Logger.getLogger(WestjetAdditionalInfoPage.class.getName());
	public WestjetAdditionalInfoPage(WebDriver driver) {
		super(driver);
		AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver,30);
		PageFactory.initElements(factory,this);
		Cls_Generic_methods.waitForElementToBeVisible(driver, button_CreateAccount);
	}
	
	@FindBy(id="address")
	private WebElement text_AddressLine1;
	
	@FindBy(id="city")
	private WebElement text_City;
	
	@FindBy(id="country")
	private WebElement select_Country;
	
	@FindBy(id="provinceStateRegion")
	private WebElement select_ProvinceStateRegion;
	
	@FindBy(id="postal-code-field")
	private WebElement text_PostalCode;
	
	@FindBy(id="homeAirport")
	private WebElement select_HomeAirport;
	
	@FindBy(id="phoneType")
	private WebElement select_PhoneType;

	@FindBy(id="phoneNumber")
	private WebElement text_PhoneNumber;
	
	@FindBy(id="accept")
	private WebElement checkbox_Accept;
	
	@FindBy(id="buttonCreate")
	private WebElement button_CreateAccount;
	
	@FindBy(id="buttonCancel")
	private WebElement button_Cancel;
	
	
	public WestjetAdditionalInfoPage typeAddressLine1(String addressLine){
		Cls_Generic_methods.sendKeys(driver, text_AddressLine1, addressLine);
		Cls_Generic_methods.pauseExecutionFor(1);
		logger.info("Typed addressLine with value "+addressLine);
		return this;
	}
	
	public WestjetAdditionalInfoPage selectCountry(String country){
		Cls_Generic_methods.selectbyText(select_Country, country);
		Cls_Generic_methods.pauseExecutionFor(1);
		logger.info("Selected country with value "+country);
		return this;
	}
	
	public WestjetAdditionalInfoPage typeCity(String city){
		Cls_Generic_methods.sendKeys(driver, text_City, city);
		Cls_Generic_methods.pauseExecutionFor(1);
		logger.info("Typed City as "+city);
		return this;
	}
	
	public WestjetAdditionalInfoPage selectProvinceStateRegion(String value ){
	Cls_Generic_methods.selectbyValue(select_ProvinceStateRegion, value);
	Cls_Generic_methods.pauseExecutionFor(1);
	logger.info("selected province as "+ value);
		return this;
	}
	
	public WestjetAdditionalInfoPage typePostalCode(String postalCode){
		Cls_Generic_methods.sendKeys(driver, text_PostalCode, postalCode);
		Cls_Generic_methods.pauseExecutionFor(1);
		logger.info("typed zip as "+ postalCode);
		return this;
	}
	
	public WestjetAdditionalInfoPage selectHomeAirport(String airport){
		Cls_Generic_methods.selectbyText(select_HomeAirport, airport);
		Cls_Generic_methods.pauseExecutionFor(1);
		logger.info("selected home airport as "+airport);
		return this;
	}
	public WestjetAdditionalInfoPage selectPhoneType(String phoneType){
		Cls_Generic_methods.selectbyText(select_PhoneType, phoneType);
		Cls_Generic_methods.pauseExecutionFor(1);
		logger.info("selected phone type as "+ phoneType);
		return this;
	}
	public WestjetAdditionalInfoPage typePhoneNumber(String phoneNumber){
		Cls_Generic_methods.sendKeys(driver, text_PhoneNumber, phoneNumber);
		Cls_Generic_methods.pauseExecutionFor(1);
		logger.info("typed phone number as "+ phoneNumber);
		return this;
	}
	
	public WestjetAdditionalInfoPage clickCreateAccount() throws Exception{
		Cls_Generic_methods.clickElement(driver, button_CreateAccount);
		Cls_Generic_methods.pauseExecutionFor(1);
		logger.info("clicked create account button");
		return this;
	}
	public WestjetAdditionalInfoPage clickAcceptTermsAndConditions() throws Exception{
		Cls_Generic_methods.scrollToElement(driver, checkbox_Accept);
		Cls_Generic_methods.moveToElement(driver, checkbox_Accept);
		Cls_Generic_methods.pauseExecutionFor(3);
		Cls_Generic_methods.clickByJS(driver, checkbox_Accept);
		//Cls_Generic_methods.clickElement(driver, checkbox_Accept);
		Cls_Generic_methods.pauseExecutionFor(1);
		logger.info("Accepted Terms and conditions");
		return this;
	}
	
	public WestjetRegistartionConfirmedPage clickSubmit() throws Exception{
		Cls_Generic_methods.moveToElement(driver, checkbox_Accept);
		Cls_Generic_methods.pauseExecutionFor(1);
		Cls_Generic_methods.clickByJS(driver, button_CreateAccount);
		Cls_Generic_methods.pauseExecutionFor(3);
		return new WestjetRegistartionConfirmedPage(driver);
	}
}

